import { randomBytes } from 'crypto'
import { readFileSync, writeFileSync } from 'fs'
import { EOL } from 'os'

const file = 'AnagraficaZliide_20230222_173147'
const domain = 'https://www.slowear.com/'
const companyId = '6fdaf6afccec44778d0b8c5e97a3d8e2'
writeFileSync(`${file}.sql`, [
    `DELETE FROM [BarcodeLinkDocs] WHERE [partition] = '${companyId}-dk-da-dk'`,
    ...readFileSync(`${file}.csv`, 'utf-8')
        .split(EOL)
        .map((line, ix) => {
            line = line.trim()
            if (line.length === 0) {
                return undefined
            }
            const [barcode, , url] = line.split(',')
            if (barcode?.length !== 13 || !Number(barcode)) {
                console.error(`./${file}:${ix + 1}: strange barcode: ${barcode}`)
                return undefined
            }
            if (!url?.startsWith(domain)) {
                console.error(`./${file}:${ix + 1}: strange URL: ${url}`)
                return undefined
            }
            return `INSERT INTO [BarcodeLinkDocs] VALUES ('${companyId}-dk-da-dk', '${barcode}', '${randomBytes(16).toString('hex')}', GETDATE(), GETDATE(), '${JSON.stringify([{
                type: 'externalEcom',
                url,
            }])}')`
        }),
    ].join(EOL)
)
